package com.shr25.test;

public class JasypTencryptPwdTest {

    public static void main(String[] args) {
        JasyptUtils.init("shr25");
        String plainText = "admin";
        String encryptedText = JasyptUtils.encryptPwd(plainText);
        System.out.println(encryptedText);
    }
}
