package com.shr25.v2.qq;

import com.shr25.common.tools.HttpUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.shr25.v2.qq.config.QQConfig;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

/**
 * qq工具类
 *
 * @author huobing
 * @date 2022年04月12日
 */
@Slf4j
public class QQUtils {

  /**
   * 跳转QQ授权页面
   * @param qqConfig
   * @param state
   * @return
   */
  public static String getAuthorizationCode(QQConfig qqConfig, String state){
    return "https://graph.qq.com/oauth2.0/authorize?response_type=code&client_id="+qqConfig.getAppid()+"&state="+state+"&redirect_uri="+ qqConfig.getRedirectUri();
  }

  /**
   * 获取AccessToken
   * @param qqConfig
   * @param code
   * @return
   */
  public static AccessToken getAccessToken(QQConfig qqConfig, String code){
    String url = "https://graph.qq.com/oauth2.0/token";
    Map<String, String> param = new HashMap<>();
    param.put("grant_type", "authorization_code");
    param.put("client_id", qqConfig.getAppid());
    param.put("client_secret", qqConfig.getAppkey());
    param.put("code", code);
    param.put("redirect_uri", qqConfig.getRedirectUri());
    param.put("fmt", qqConfig.getFmt());

    AccessToken accessToken = null;
    try{
      String json =  HttpUtils.doGet(url, param);
      accessToken = JSON.parseObject(json, AccessToken.class);
    }catch (Exception e){
      log.error("获取qq用户AccessToken失败", e);
      new RuntimeException("获取qq用户AccessToken失败");
    }
    return accessToken;
  }

  /**
   * 获取QQ用户的openid
   * @param qqConfig
   * @param accessToken
   * @return
   */
  public static String getOpenid(QQConfig qqConfig, AccessToken accessToken){
    String url = "https://graph.qq.com/oauth2.0/me";
    Map<String, String> param = new HashMap<>();
    param.put("access_token", accessToken.access_token);
    param.put("fmt", qqConfig.getFmt());

    String openid = null;
    try{
      String json =  HttpUtils.doGet(url, param);
      JSONObject jsonObject = JSONObject.parseObject(json);
      openid = jsonObject.getString("openid");
    }catch (Exception e){
      log.error("获取qq用户openid失败", e);
    }
    return openid;
  }

  /**
   * 通过openid获取QQ用户信息
   * @param qqConfig
   * @param accessToken
   * @param openid
   * @return
   */
  public static QQUserInfo getUserInfo(QQConfig qqConfig, AccessToken accessToken, String openid){
    String url = "https://graph.qq.com/user/get_user_info";
    Map<String, String> param = new HashMap<>();
    param.put("access_token", accessToken.access_token);
    param.put("oauth_consumer_key", qqConfig.getAppid());
    param.put("openid", openid);

    QQUserInfo qqUserInfo = null;
    try{
      String json =  HttpUtils.doGet(url, param);
      qqUserInfo = JSON.parseObject(json, QQUserInfo.class);
      qqUserInfo.setOpenid(openid);
    }catch (Exception e){
      log.error("获取qq用户信息失败", e);
    }
    return qqUserInfo;
  }

  /**
   * 获取qq用户信息
   * @param qqConfig
   * @param code
   * @return
   */
  public static QQUserInfo getUserInfo(QQConfig qqConfig, String code){
    AccessToken accessToken = getAccessToken(qqConfig, code);
    String openid = getOpenid(qqConfig, accessToken);
    return getUserInfo(qqConfig, accessToken, openid);
  }

  public static void main(String[] args) {
    String json ="{\n" +
        "\"ret\": 0,\n" +
        "\"msg\": \"\",\n" +
        "\"is_lost\":0,\n" +
        "\"nickname\": \"抓在手里的阳光\",\n" +
        "\"gender\": \"男\",\n" +
        "\"gender_type\": 2,\n" +
        "\"province\": \"广东\",\n" +
        "\"city\": \"深圳\",\n" +
        "\"year\": \"1990\",\n" +
        "\"constellation\": \"\",\n" +
        "\"figureurl\": \"http:\\/\\/qzapp.qlogo.cn\\/qzapp\\/101998830\\/98B7DDC3D3ED7207444A36F90A9DDDED\\/30\",\n" +
        "\"figureurl_1\": \"http:\\/\\/qzapp.qlogo.cn\\/qzapp\\/101998830\\/98B7DDC3D3ED7207444A36F90A9DDDED\\/50\",\n" +
        "\"figureurl_2\": \"http:\\/\\/qzapp.qlogo.cn\\/qzapp\\/101998830\\/98B7DDC3D3ED7207444A36F90A9DDDED\\/100\",\n" +
        "\"figureurl_qq_1\": \"http://thirdqq.qlogo.cn/g?b=oidb&k=az5vHmH9bk4J5EzvJXgfqQ&s=40&t=1556420720\",\n" +
        "\"figureurl_qq_2\": \"http://thirdqq.qlogo.cn/g?b=oidb&k=az5vHmH9bk4J5EzvJXgfqQ&s=100&t=1556420720\",\n" +
        "\"figureurl_qq\": \"http://thirdqq.qlogo.cn/g?b=oidb&k=az5vHmH9bk4J5EzvJXgfqQ&s=640&t=1556420720\",\n" +
        "\"figureurl_type\": \"1\",\n" +
        "\"is_yellow_vip\": \"0\",\n" +
        "\"vip\": \"0\",\n" +
        "\"yellow_vip_level\": \"0\",\n" +
        "\"level\": \"0\",\n" +
        "\"is_yellow_year_vip\": \"0\"\n" +
        "}";
    QQUserInfo qqUserInfo = JSON.parseObject(json, QQUserInfo.class);
    System.out.println(JSONObject.toJSON(qqUserInfo));
  }

}
