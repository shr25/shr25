package com.shr25.v2.common.base;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.shr25.v2.common.support.ConvertUtil;

/**
 * 公共service实现
 *
 * @author huobing
 * @date 2022-03-09 22:02:24
 */
public class BaseServiceImpl<M extends BaseMapper<T>, T> extends ServiceImpl< M, T> implements BaseService<T> {

    @Override
    public Boolean removeByIds(String ids) {
        return removeByIds(ConvertUtil.toListLongArray(ids));
    }
}
