package com.shr25.v2.common.exception;

/**
 * 演示模式异常
* @ClassName: DemoModeException
* @author fuce
* @date 2019-11-08 15:45
*
 */
public class ServiceException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public ServiceException() {
    }

    public ServiceException(String message) {
        super(message);
    }
}
