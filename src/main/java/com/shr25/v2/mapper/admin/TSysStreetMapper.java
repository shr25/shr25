
package com.shr25.v2.mapper.admin;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shr25.v2.model.admin.TSysStreet;

/**
 * 街道设置Mapper接口
 * 
 * @author zhaonz
 * @date 2021-08-05
 */
public interface TSysStreetMapper extends BaseMapper<TSysStreet>
{

}
