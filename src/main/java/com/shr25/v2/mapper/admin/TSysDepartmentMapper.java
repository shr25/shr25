
package com.shr25.v2.mapper.admin;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shr25.v2.model.admin.TSysDepartment;

/**
 * 部门Mapper接口
 * 
 * @author zhaonz
 * @date 2021-08-05
 */
public interface TSysDepartmentMapper extends BaseMapper<TSysDepartment> {

}
