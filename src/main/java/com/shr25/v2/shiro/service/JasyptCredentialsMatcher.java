package com.shr25.v2.shiro.service;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.jasypt.encryption.StringEncryptor;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 身份校验核心类--Jasypt
 *
 * @author huobing
 * @className: JasyptCredentialsMatcher
 * @date 2022年04月12日
 */
public class JasyptCredentialsMatcher extends HashedCredentialsMatcher {
  @Autowired
  public StringEncryptor stringEncryptor;

  @Override
  public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) {
    String password = stringEncryptor.decrypt(info.getCredentials().toString());
    return password.equals(new String((char[]) token.getCredentials()));
  }
}
