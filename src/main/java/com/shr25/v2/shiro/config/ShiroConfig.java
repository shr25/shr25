package com.shr25.v2.shiro.config;

import at.pollux.thymeleaf.shiro.dialect.ShiroDialect;
import cn.hutool.core.collection.CollectionUtil;
import com.shr25.v2.shiro.service.CORSAuthenticationFilter;
import com.shr25.v2.shiro.service.JasyptCredentialsMatcher;
import com.shr25.v2.shiro.service.ShiroSession;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.mgt.RememberMeManager;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.session.mgt.SessionManager;
import org.apache.shiro.session.mgt.eis.SessionDAO;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.CookieRememberMeManager;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.servlet.Cookie;
import org.apache.shiro.web.servlet.SimpleCookie;
import org.crazycake.shiro.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Filter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 权限配置文件
 * 
 * @ClassName: ShiroConfiguration
 * @author fuce
 * @date 2018年8月25日
 *
 */
@Configuration
public class ShiroConfig {

	@Autowired
	private RedisProperties redisProperties;

	/**
	 * 这是shiro的大管家，相当于mybatis里的SqlSessionFactoryBean
	 * 
	 * @param securityManager
	 * @return
	 */
	@Bean
	public ShiroFilterFactoryBean shiroFilterFactoryBean(org.apache.shiro.mgt.SecurityManager securityManager) {
		ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
		shiroFilterFactoryBean.setSecurityManager(securityManager);
		// 登录
		shiroFilterFactoryBean.setLoginUrl("/admin/login");
		// 首页
//		shiroFilterFactoryBean.setSuccessUrl("/");
		// 错误页面，认证不通过跳转
		shiroFilterFactoryBean.setUnauthorizedUrl("/error/403");
		// 页面权限控制
		shiroFilterFactoryBean.setFilterChainDefinitionMap(ShiroFilterMapFactory.shiroFilterMap());

		// 自定义拦截器
		Map<String, Filter> customFilterMap = new LinkedHashMap<>();
		customFilterMap.put("corsAuthenticationFilter", new CORSAuthenticationFilter());
		shiroFilterFactoryBean.setFilters(customFilterMap);

		return shiroFilterFactoryBean;
	}

	/**
	 * web应用管理配置
	 * 
	 * @param shiroRealm
	 * @param manager
	 * @return
	 */
	@Bean
	public DefaultWebSecurityManager securityManager(AuthorizingRealm[] shiroRealm,
			RememberMeManager manager) {
		DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
		securityManager.setCacheManager(cacheManager());
		securityManager.setRememberMeManager(manager);// 记住Cookie
		Collection<Realm> realms = new ArrayList<Realm>(shiroRealm.length);
		for (int i = 0; i < shiroRealm.length; i++) {
			realms.add(shiroRealm[i]);
		}
		securityManager.setRealms(realms);
		securityManager.setSessionManager(sessionManager());
		return securityManager;
	}

	/**
	 * 自定义的 shiro session 缓存管理器，用于跨域等情况下使用 token 进行验证，不依赖于sessionId
	 * 
	 * @return
	 */
	@Bean
	public SessionManager sessionManager() {
		// 将我们继承后重写的shiro session 注册
		ShiroSession shiroSession = new ShiroSession();
		shiroSession.setSessionDAO(getSessionDao());
		shiroSession.setCacheManager(cacheManager());
		return shiroSession;
	}

	@Bean
	public SessionDAO getSessionDao(){
		// 如果后续考虑多tomcat部署应用，可以使用shiro-redis开源插件来做session 的控制，或者nginx 的负载均衡
		RedisSessionDAO sessionDAO = new RedisSessionDAO();
		sessionDAO.setRedisManager(redisManager());
		return sessionDAO;
	}

	/**
	 * 缓存配置
	 * @return
	 */
	@Bean
	public CacheManager cacheManager() {
		RedisCacheManager cacheManager = new RedisCacheManager();
		cacheManager.setRedisManager(redisManager());
		return cacheManager;
	}

	/**
	 * 缓存配置
	 * @return
	 */
	@Bean
	public IRedisManager redisManager() {
		if(redisProperties.getSentinel() != null){
			RedisSentinelManager redisSentinelManager = new RedisSentinelManager();
			redisSentinelManager.setHost(CollectionUtil.join(redisProperties.getSentinel().getNodes(), ","));
			redisSentinelManager.setPassword(redisProperties.getSentinel().getPassword());
			redisSentinelManager.setDatabase(redisProperties.getDatabase());
			redisSentinelManager.setMasterName(redisProperties.getSentinel().getMaster());
			return redisSentinelManager;
		}else if(redisProperties.getCluster() != null){
			RedisClusterManager redisClusterManager = new RedisClusterManager();
			redisClusterManager.setHost(CollectionUtil.join(redisProperties.getCluster().getNodes(), ","));
			redisClusterManager.setDatabase(redisProperties.getDatabase());
			return redisClusterManager;
		}else{
			RedisManager redisManager = new RedisManager();
			redisManager.setHost(redisProperties.getHost());
			redisManager.setPort(redisProperties.getPort());
			redisManager.setPassword(redisProperties.getPassword());
			redisManager.setDatabase(redisProperties.getDatabase());
			return redisManager;
		}
	}
	
	/**
	 * 加密算法
	 * 
	 * @return
	 */
	@Bean
	public JasyptCredentialsMatcher hashedCredentialsMatcher() {
		JasyptCredentialsMatcher hashedCredentialsMatcher = new JasyptCredentialsMatcher();
		return hashedCredentialsMatcher;
	}

	/**
	 * 记住我的配置
	 * 
	 * @return
	 */
	@Bean
	public RememberMeManager rememberMeManager() {
		Cookie cookie = new SimpleCookie("rememberMe");
		cookie.setHttpOnly(true);// 通过js脚本将无法读取到cookie信息
		cookie.setMaxAge(60 * 60 * 24);// cookie保存一天
		CookieRememberMeManager manager = new CookieRememberMeManager();
		manager.setCookie(cookie);
		return manager;
	}

	/**
	 * 启用shiro方言，这样能在页面上使用shiro标签
	 * 
	 * @return
	 */
	@Bean
	public ShiroDialect shiroDialect() {
		return new ShiroDialect();
	}

	/**
	 * 启用shiro注解 加入注解的使用，不加入这个注解不生效
	 */
	@Bean
	public AuthorizationAttributeSourceAdvisor getAuthorizationAttributeSourceAdvisor(
			org.apache.shiro.mgt.SecurityManager securityManager) {
		AuthorizationAttributeSourceAdvisor advisor = new AuthorizationAttributeSourceAdvisor();
		advisor.setSecurityManager(securityManager);
		return advisor;
	}

}
