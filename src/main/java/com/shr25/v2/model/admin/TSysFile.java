
package com.shr25.v2.model.admin;

import com.baomidou.mybatisplus.annotation.TableName;
import com.shr25.v2.common.base.BaseCreateTimeEntity;
import lombok.Data;

/**
 * 文件信息对象 t_sys_file
 *
 * @author huobing
 * @date 2022-03-10
 */
@Data
@TableName("t_sys_file")
public class TSysFile extends BaseCreateTimeEntity
{
    /** 文件名字 */
    private String fileName;

    /** 桶名 */
    private String bucketName;

    /** 文件大小 */
    private Long fileSize;

    /** 后缀 */
    private String fileSuffix;

    /** 文件路径 */
    private String filePath;



    public TSysFile() {
        super();
    }

    public TSysFile(String fileName, String bucketName, Long fileSize, String fileSuffix) {
        this.fileName = fileName;
        this.bucketName = bucketName;
        this.fileSize = fileSize;
        this.fileSuffix = fileSuffix;
    }

    public TSysFile(String fileName, Long fileSize, String fileSuffix, String filePath) {
        this.fileName = fileName;
        this.fileSize = fileSize;
        this.fileSuffix = fileSuffix;
        this.filePath = filePath;
    }
}
