
package com.shr25.v2.model.admin;

import com.baomidou.mybatisplus.annotation.TableName;
import com.shr25.v2.common.base.BaseDateTimeEntity;
import lombok.Data;
/**
 * 省份对象 t_sys_province
 *
 * @author huobing
 * @date 2022-03-10
 */
@Data
@TableName("t_sys_province")
public class TSysProvince extends BaseDateTimeEntity
{
    /** 省份代码 */
    private String provinceCode;

    /** 省份名称 */
    private String provinceName;

    /** 简称 */
    private String shortName;

    /** 经度 */
    private String lng;

    /** 纬度 */
    private String lat;

    /** 排序 */
    private Integer sort;

    /** 状态 */
    private Integer dataState;
}
