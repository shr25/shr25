
package com.shr25.v2.model.admin;

import com.baomidou.mybatisplus.annotation.TableName;
import com.shr25.v2.common.base.BaseDateTimeEntity;
import lombok.Data;
/**
 * 角色对象 t_sys_role
 *
 * @author huobing
 * @date 2022-03-10
 */
@Data
@TableName("t_sys_role")
public class TSysRole extends BaseDateTimeEntity
{
    /** 角色名称 */
    private String name;
}
