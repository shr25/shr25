
package com.shr25.v2.model.admin;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.shr25.v2.common.base.BaseDateTimeEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
/**
 * 用户对象 t_sys_user
 *
 * @author huobing
 * @date 2022-03-10
 */
@TableName("t_sys_user")
@ApiModel(value = "TSysUser", description = "用户表")
@Data
public class TSysUser extends BaseDateTimeEntity
{
    public static final long serialVersionUID = 1L;

    /** 用户账号 */
    @ApiModelProperty(value = "用户账号")
    private String username;

    /** 用户密码 */
    @ApiModelProperty(value = "用户密码")
    private String password;

    /** 昵称 */
    @ApiModelProperty(value = "昵称")
    private String nickname;

    /** 真实姓名 */
    @ApiModelProperty(value = "真实姓名")
    private String name;

    /** 用户类型 */
    @ApiModelProperty(value = "用户类型")
    private String userType;

    /** 用户邮箱 */
    @ApiModelProperty(value = "用户邮箱")
    private String email;

    /** 手机号码 */
    @ApiModelProperty(value = "手机号码")
    private String phonenumber;

    /** qq */
    @ApiModelProperty(value = "qq")
    private String qq;

    /** qq统一openid */
    @ApiModelProperty(value = "qq统一openid")
    private String qqOpenid;

    /** qq昵称 */
    @ApiModelProperty(value = "qq昵称")
    private String qqName;

    /** qq头像 */
    @ApiModelProperty(value = "qq头像")
    private String qqFigureurl;

    /** 一键加群html */
    @ApiModelProperty(value = "一键加群html")
    private String addQqGroupHtml;

    /** 用户性别 0=男,1=女,2=未知*/
    @ApiModelProperty(value = "用户性别")
    private String sex;

    /** 用户头像 */
    @ApiModelProperty(value = "用户头像")
    private String avatar;

    /** 部门id */
    @ApiModelProperty(value = "部门id")
    private Long depId;

    /** 岗位id */
    @ApiModelProperty(value = "岗位id")
    private Long posId;

    /** 部门名称 */
    @TableField(exist = false)
    private String depName;

    /** 岗位名称 */
    @TableField(exist = false)
    private String posName;

    /** 收款账号 */
    @ApiModelProperty(value = "收款账号")
    private String collectionAccount;

    /** 推荐人id */
    @ApiModelProperty(value = "推荐人id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long extensionId;

    /** 推荐人名称 */
    @ApiModelProperty(value = "推荐人名称")
    private String extensionName;

    /** 兼容shiro */
    public Long getId(){
        return super.getId();
    }
}
