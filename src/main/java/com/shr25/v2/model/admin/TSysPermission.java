
package com.shr25.v2.model.admin;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.shr25.v2.common.base.BaseDateTimeEntity;
import lombok.Data;
/**
 * 权限对象 t_sys_permission
 *
 * @author huobing
 * @date 2022-03-10
 */
@Data
@TableName("t_sys_permission")
public class TSysPermission extends BaseDateTimeEntity
{
    /** 权限名称 */
    private String name;

    /** 权限描述 */
    private String descripion;

    /** 授权链接 */
    private String url;

    /** 是否跳转 0 不跳转 1跳转 */
    private Integer isBlank;

    /** 父节点id */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long pid;

    /** 权限标识 */
    private String perms;

    /** 类型   0：目录   1：菜单   2：按钮 */
    private Integer type;

    /** 菜单图标 */
    private String icon;

    /** 排序 */
    private Integer orderNum;

    /** 是否可见 */
    private Integer visible;

    @TableField(exist = false)
    private Integer childCount;
}
