
package com.shr25.v2.model.admin;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.shr25.v2.common.base.BaseCreateTimeEntity;
import lombok.Data;

/**
 * 日志记录对象 t_sys_oper_log
 *
 * @author huobing
 * @date 2022-03-10
 */
@Data
@TableName("t_sys_oper_log")
public class TSysOperLog extends BaseCreateTimeEntity
{
    /** 标题 */
    private String title;

    /** 方法 */
    private String method;

    /** url */
    private String operUrl;

    /** 参数 */
    private String operParam;

    /**  */
    private String errorMsg;

    /** 操作人 */
    @TableField(exist = false)
    private String operName;
}
