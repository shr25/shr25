
package com.shr25.v2.model.admin;

import com.baomidou.mybatisplus.annotation.TableName;
import com.shr25.v2.common.base.BaseDateTimeEntity;
import lombok.Data;

/**
 * 字典数据对象 t_sys_dict_data
 *
 * @author huobing
 * @date 2022-03-10
 */
@Data
@TableName("t_sys_dict_data")
public class TSysDictData extends BaseDateTimeEntity
{
    /** 字典排序 */
    private Integer dictSort;

    /** 字典标签 */
    private String dictLabel;

    /** 字典键值 */
    private String dictValue;

    /** 字典类型 */
    private String dictType;

    /** 样式属性（其他样式扩展） */
    private String cssClass;

    /** 表格回显样式 */
    private String listClass;

    /** 是否默认（Y是 N否） */
    private String isDefault;

    /** 状态（0正常 1停用） */
    private String status;

    /** 备注 */
    private String remark;
}
