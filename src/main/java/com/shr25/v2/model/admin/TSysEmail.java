
package com.shr25.v2.model.admin;

import com.baomidou.mybatisplus.annotation.TableName;
import com.shr25.v2.common.base.BaseDateTimeEntity;
import lombok.Data;

/**
 * 电子邮件对象 t_sys_email
 *
 * @author huobing
 * @date 2022-03-10
 */
@Data
@TableName("t_sys_email")
public class TSysEmail extends BaseDateTimeEntity
{
    /** 接收人电子邮件 */
    private String receiversEmail;

    /** 邮件标题 */
    private String title;

    /** 内容 */
    private String content;
}